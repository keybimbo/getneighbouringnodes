private List<PathNode> GetNeighbouringNodes(PathNode node, bool[,] grid, Point end)
    {
        List<PathNode> neighboruingNodes = new List<PathNode>();

        for (int i = 0; i < 4 * _allowDiaganalMovment; i++) //loop through evey possible node
        {
            if (ValidNode(node.GridLocation + _directions[i], grid)) //check each node to see if its valid or not
            {
                neighboruingNodes.Add(new PathNode(_movementCosts[i] + node.G,
                    TargetDistance(node.GridLocation + _directions[i], end), node.GridLocation, node.GridLocation + _directions[i])); //add the new node
            }
        }

        return neighboruingNodes;
    }